//
//  ListViewController.swift
//  Komica2.0
//
//  Created by nwfmbin2 on 2020/07/25.
//  Copyright © 2020 bin.work. All rights reserved.
//

import UIKit

/// 首頁列表
class ListViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .blue
        appEnviroment.makeRequest { (result) in
            switch result{
            case .failure(let err):
                track(err)
            case .success(let s):
                track(s)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
