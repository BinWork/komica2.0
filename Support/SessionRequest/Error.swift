//
//  Error.swift
//  Komica2.0
//
//  Created by nwfmbin2 on 2020/07/25.
//  Copyright © 2020 bin.work. All rights reserved.
//

import Foundation

enum RequestError: Error{
    case InValidURL(String)
    case ConnectionProblem(String)
    case ParsingFailed
    case RequestError(Error)
    case ResponseIsNil
    case DataIsNil
}


