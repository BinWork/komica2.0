//
//  Enviroment.swift
//  Komica2.0
//
//  Created by nwfmbin2 on 2020/07/25.
//  Copyright © 2020 bin.work. All rights reserved.
//

import Foundation
import UIKit

/// App目前環境
let appEnviroment: AppEnviroment = AppEnviroment(main: .Komica)

struct AppEnviroment{
    
    var main: DomainEnum
    
    internal enum DomainEnum{
        case Komica
        case Komica2
        
        var urlString: String{
            switch self {
            case .Komica:
                return "https://komica.org/m/"
            case .Komica2:
                return "https://komica2.net/m/"
            }
        }
    }
    
    init(main: DomainEnum) {
        self.main = main
    }
    
//    func makeRequest<T>(strURL: String, structure: T, completion: (Result< T, RequestError>) -> Void) {
//        guard let url = URL(string: strURL) else {
//            completion(.failure(.InValidURL(strURL)))
//            return
//        }
//
//        makeHTTPRequest(with: url) { (result) in
//            completion(result)
//        }
//
//    }
//
//    private func makeHTTPRequest<T>(with url: URL, completion: (Result< T, RequestError>) -> Void){
//        let task = URLSession.shared.dataTask(with: url) {(requestData, requestRes, requestErr) in
//
//            if let err =  requestErr{
//                completion(.failure(.RequestError(err)))
//                return
//            }
//
//
//            guard let data = requestData else{
//                completion(.failure(.DataIsNil))
//                return
//            }
//
//
//
//            do{
//                let contents = String(data: data, encoding: .utf8)
//                track(contents)
//
//            }catch{
//                completion(.failure(.ParsingFailed))
//            }
//        }
//
//        task.resume()
//    }
    func makeRequest(completion: @escaping (Result< String, RequestError>) -> Void) {
        let strURL = main.urlString
        guard let url = URL(string: main.urlString) else {
                completion(.failure(.InValidURL(strURL)))
                return
            }
    
        makeHTTPRequest(with: url) { (result) in
            switch result{
            case .success(let s):
                completion(.success(s))
            case .failure(let err):
                completion(.failure(err))
            }
        }
    
    }
    
    private func makeHTTPRequest(with url: URL, completion: @escaping (Result< String, RequestError>) -> Void){
        let task = URLSession.shared.dataTask(with: url) {(requestData, requestRes, requestErr) in

            if let err =  requestErr{
                completion(.failure(.RequestError(err)))
                return
            }


            guard let data = requestData else{
                completion(.failure(.DataIsNil))
                return
            }



            do{
                if let contents = String(data: data, encoding: .utf8){
                    completion(.success(contents))
                }else{
                    completion(.failure(.ParsingFailed))
                }

            }catch{
                completion(.failure(.ParsingFailed))
            }
        }

        task.resume()
    }
}
