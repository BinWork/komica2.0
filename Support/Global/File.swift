//
//  File.swift
//  Komica2.0
//
//  Created by nwfmbin2 on 2020/07/25.
//  Copyright © 2020 bin.work. All rights reserved.
//

import Foundation
import UIKit

/// Device screen‘s Rect
let SCREEN_RECT: CGRect = UIScreen.main.bounds
/// Device screen's Size
let SCREEN_SIZE: CGSize = SCREEN_RECT.size
