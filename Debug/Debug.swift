//
//  Debug.swift
//  Komica2.0
//
//  Created by nwfmbin2 on 2020/07/25.
//  Copyright © 2020 bin.work. All rights reserved.
//

import Foundation

public func track(_ obj: Any?,
                  file: String = #file, function: String = #function, line: Int = #line ) {
    print("🐯start===")
    print("\(obj) called from \(function) \(file):\(line)")
    print("🐯start===")
}
